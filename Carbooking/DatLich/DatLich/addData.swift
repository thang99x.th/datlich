//
//  addData.swift
//  DatLich
//
//  Created by Pham Thang on 3/9/19.
//  Copyright © 2019 Pham Thang. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class AddDataService {
    static let shared = AddDataService() 
    
    func addTempData() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appdelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "TblBooking", in: context)
        let newValue = NSManagedObject(entity: entity!, insertInto: context)
        
        for i in 0...2 {
            guard let ngaydi = dateFormatter.date(from: "07-03-2019") else {return}
            guard let ngayden = dateFormatter.date(from: "09-03-2019") else {return}
            newValue.setValue("Hà Nội", forKey: "diemdi")
            newValue.setValue("Đà Nẵng", forKey: "diemden")
            newValue.setValue("Du lich", forKey: "lydo")
            newValue.setValue(ngaydi, forKey: "ngaydi")
            newValue.setValue(ngayden, forKey: "ngayden")
            do {
                try context.save()
                print("Lưu dữ liệu thành công")
            } catch  {
                print("Lưu dữ liệu không thành công")
            }
        }
    
        
    }
    
    func loadData() -> [TblBooking] {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appdelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TblBooking")
        
        var dataResult: [TblBooking] = []
        
        do {
            dataResult = try context.fetch(TblBooking.fetchRequest())
            print(dataResult)
        } catch {
            
        }
        return dataResult
    }
}


