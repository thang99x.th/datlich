//
//  ViewController.swift
//  DatLich
//
//  Created by Pham Thang on 22/12/2018.
//  Copyright © 2018 Pham Thang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var sgmControl: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        sgmControl.selectedSegmentIndex = 0
        sgmControl.sendActions(for: .valueChanged)
        
        //them du lieu vao coredata
//        let service = AddDataService()
//        service.addTempData()
        AddDataService.shared.addTempData()
        
        
        
    
    }
    @IBAction func btnChangeView(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0
        {
            UIView.animate(withDuration: 0.5) {
                self.View2.isHidden = true
                self.View1.isHidden = false
            }
        }
        if sender.selectedSegmentIndex == 1
        {
            UIView.animate(withDuration: 0.5, animations: {
                self.View2.isHidden = false
                self.View1.isHidden = true
            })
        }
    }
    

}

