//
//  CalendarViewController.swift
//  DatLich
//
//  Created by Pham Thang on 22/12/2018.
//  Copyright © 2018 Pham Thang. All rights reserved.
//

import UIKit
import JTAppleCalendar
import CoreData

class CalendarViewController: UIViewController {
    @IBOutlet weak var cal: JTAppleCalendarView!
    @IBOutlet weak var carlendarview: JTAppleCalendarView!
    @IBOutlet weak var lblMonth: UILabel!
    let formatter = DateFormatter();
    let outsideMonthcolor = UIColor.gray
    let monthColor = UIColor.black
    let selectMonthColor = UIColor.black
    let currentDateselect = UIColor.white
    
    var dataResult : [TblBooking] = []
    
    
    
    let date = Date();
    var dateforCust = Date();
    override func viewDidLoad() {
        super.viewDidLoad()
        cal.calendarDelegate = self
        cal.calendarDataSource = self
        // Do any additional setup after loading the view.
        formatter.dateFormat = "LLLL"
        let nameOfMonth = formatter.string(from: date)
        let CurrentYear = Calendar.current.component(.year, from: date)
        lblMonth.text = nameOfMonth + " - " + String(CurrentYear)
        setupCarlendarView()
        
        dataResult = AddDataService.shared.loadData()
    }
    @IBAction func btnNextMonth(_ sender: Any) {
        dateforCust = Calendar.current.date(byAdding: .month, value: 1, to: dateforCust)!
        // cal.reloadData(withanchor: dateforCust, completionHandler: nil)
//        cal.scrollToDate(dateforCust, triggerScrollToDateDelegate: false, animateScroll: true, preferredScrollPosition: nil, extraAddedOffset: 0, completionHandler: nil)
        self.cal.scrollToSegment(.next)
    }
    @IBAction func btnPrevMonth(_ sender: Any) {
        self.cal.scrollToSegment(.previous)
    }
    func setupCarlendarView(){
        carlendarview.minimumLineSpacing = 0
        carlendarview.minimumInteritemSpacing = 0
    }
    func hanldeceltextcolor (view:JTAppleCell?,cellstate:CellState){
        guard let validcell = view as? CellDay else {return}
        if cellstate.isSelected{
            validcell.lblDate.textColor = selectMonthColor
        }
        else
        {
            if cellstate.dateBelongsTo == .thisMonth {
                validcell.lblDate.textColor = monthColor
            }
            else{
                validcell.lblDate.textColor = outsideMonthcolor
            }
        }
    }
    func hanldecelselected (view:JTAppleCell?,cellstate:CellState){
        guard let validcell = view as? CellDay else {return}
        if validcell.isSelected{
            validcell.selectedview.isHidden = false
        }
        else
        {
            validcell.selectedview.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CalendarViewController: JTAppleCalendarViewDataSource{
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
    
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "dd MM yyyy"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startdate = formatter.date(from: "01 01 2019")
        let enddate = formatter.date(from: "01 12 2019")
        
        let comp : DateComponents = Calendar.current.dateComponents([.year,.month], from: date)
//        let startOfMonth = Calendar.current.date(from: comp)
        
        var comp2 = DateComponents()
        comp2.month = 1
        comp2.day = -1
//        let endOfMonth = Calendar.current.date(byAdding: comp2, to: startOfMonth!)
//        let endOfMonth = formatter.date(from: "01 12 2020")
        
//        let param = ConfigurationParameters(startDate: (startdate)!, endDate: (enddate)!)
        let param = ConfigurationParameters(startDate: (startdate)!, endDate: enddate!, numberOfRows: 4, generateInDates: InDateCellGeneration.forAllMonths, generateOutDates: OutDateCellGeneration.tillEndOfRow, firstDayOfWeek: .monday)
        return param
    }
}
extension CalendarViewController: JTAppleCalendarViewDelegate{
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CellDay", for: indexPath) as! CellDay
        
        let isHassame = dataResult.contains { (obj1) -> Bool in
           return obj1.ngaydi?.compare(with: date, only: .day) == 0
            && obj1.ngaydi?.compare(with: date, only: .month) == 0
        }
        
        if(isHassame)
        {
            cell.lblDate.text = cellState.text
            //cell.lblDate.textColor = UIColor.red
            cell.contentView.backgroundColor = UIColor.red
        }
        else{
            cell.lblDate.text = cellState.text
            cell.lblDate.textColor = UIColor.black
        }
        
        
        
        hanldecelselected(view: cell, cellstate: cellState)
        hanldeceltextcolor(view: cell, cellstate: cellState)
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        hanldecelselected(view: cell, cellstate: cellState)
        hanldeceltextcolor(view: cell, cellstate: cellState)
    }
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        hanldecelselected(view: cell, cellstate: cellState)
        hanldeceltextcolor(view: cell, cellstate: cellState)
    }
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = Calendar.current.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = Calendar.current.component(.year, from: startDate)
        lblMonth.text = monthName + " - " + String(year)
    }
}
extension UIColor{
    convenience init(colorWithHexValue value:Int, alpha:CGFloat = 1.0 )
    {
        self.init(
            red: CGFloat((value & 0xFF0000)>>16)/255.0,
            green: CGFloat((value & 0xFF0000)>>16)/255.0,
            blue: CGFloat((value & 0xFF0000)>>16)/255.0,
            alpha:alpha
        )
    }
}
extension Date{
    func totalDistance(from date: Date, resultIn component: Calendar.Component) -> Int? {
        return Calendar.current.dateComponents([component], from: self, to: date).value(for: component)
    }
    
    func compare(with date: Date, only component: Calendar.Component) -> Int {
        let days1 = Calendar.current.component(component, from: self)
        let days2 = Calendar.current.component(component, from: date)
        return days1 - days2
    }
    
    func hasSame(_ component: Calendar.Component, as date: Date) -> Bool {
        return self.compare(with: date, only: component) == 0
    }
}
