//
//  ViewController.swift
//  Carbooking
//
//  Created by Pham Thang on 19/05/2018.
//  Copyright © 2018 Pham Thang. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var txtTen: UITextField!
    @IBOutlet weak var txtpass: UITextField!
    var quotes = [TblUser]()
    @IBAction func btntaomoi(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let newEntry = TblUser(context:context)
        newEntry.user = txtTen.text
        newEntry.pass = txtpass.text
        newEntry.permission = "User"
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        let alert = UIAlertController(title: "Thông Báo", message: "Bạn đã tạo thành công tài khoản", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "ok", style: .default, handler: {(ACTION)->Void in print("OK")})
        alert.addAction(ok)
        self.present(alert,animated: true, completion: nil)
    }
    @IBAction func btnOk(_ sender: Any) {
        //storyboard
        let sb = UIStoryboard(name: "Main", bundle: nil)
        //tao man hinh can chuyển
        let calendarview = sb.instantiateViewController(withIdentifier: "Calendarview") as! CalendarViewController
        //mo man hinh
        self.navigationController?.pushViewController(calendarview, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

