//
//  TongquanUITableView.swift
//  Carbooking
//
//  Created by Pham Thang on 07/06/2018.
//  Copyright © 2018 Pham Thang. All rights reserved.
//

import UIKit
class TongquanUITableView: UITableView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */   
    var reuseIdentfier = "tbvCell"
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var quotes : [TblUser] = []
    func fetchData() {
        do {
            quotes = try context.fetch(TblUser.fetchRequest())
            print(quotes)
            DispatchQueue.main.async {
                self.tbviewtongquan.reloadData()
            }
        } catch {
            print("Không lấy được data")
        }
    }
    override func numberOfRows(inSection section: Int) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: danhsachTableViewCell.reuseIdentifier, for: indexPath) as? danhsachTableViewCell else {
            fatalError("Khong lay duoc danh sach")
        }
        
        // fetch quote
        let quote = quotes[indexPath.row]
        // Configure the cell...
        cell.lblUser.text = quote.user
        cell.lblPass.text = quote.pass
        cell.lblQuyen.text = quote.permission
        
        return cell
    }
    override func tbviewtongquan(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return quotes.count
    }
    
}
